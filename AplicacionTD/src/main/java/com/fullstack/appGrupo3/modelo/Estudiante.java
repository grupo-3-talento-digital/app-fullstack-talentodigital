package com.fullstack.appGrupo3.modelo;





import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import javax.validation.constraints.Size;





public class Estudiante {
	
	
	
	private int id_estudiante;
	@NotEmpty(message = "Debe rellenar el campo")
	@Size( max = 30, message ="Error: El campo debe tener entre 1 y 30 caracteres" )
	private String nombre;
	@NotEmpty(message = "Debe rellenar el campo")
	@Size( max = 30, message ="Error: El campo debe tener entre 1 y 30 caracteres" )
	private String apellido;
	@NotEmpty(message="Debe rellenar el campo")
	@Email
	private String email;
	@NotEmpty(message = "Debe elegir una ciudad")
	private String ciudad;
	
	//REVISAR TIPO
	@NotEmpty(message = "Debe seleccionar al menos un lenguaje")
	private List<String> lenguajePro;
	@NotEmpty(message = "Debe seleccionar un sistema operativo")
	private String sistemaOp;
	
		
	public int getId_estudiante() {
		return id_estudiante;
	}

	public void setId_estudiante(int id_estudiante) {
		this.id_estudiante = id_estudiante;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre)  {
		
		this.nombre = nombre;
		
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido)  {
		
		this.apellido = apellido;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad)  {
		
		this.ciudad = ciudad;
		
	}

	public List<String> getLenguajePro() {
		
		return lenguajePro;
	}

	public void setLenguajePro(List<String> lenguajePro)  {
		
		this.lenguajePro = lenguajePro;
		
	}

	public String getSistemaOp() {
		return sistemaOp;
	}

	public void setSistemaOp(String sistemaOp) {
		this.sistemaOp = sistemaOp;
		
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email)  {
		this.email = email;
	}
	

	@Override
	public String toString() {
		return "Estudiante [id_estudiante=" + id_estudiante + ", nombre=" + nombre + ", apellido=" + apellido
				+  ", email=" + email +", ciudad=" + ciudad + ", lenguajePro=" + lenguajePro + ", sistemaOp=" + sistemaOp + "]";
	}
	
	
}
