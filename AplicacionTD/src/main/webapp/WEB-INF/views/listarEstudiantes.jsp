<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Lista de estudiantes</title>

<link href="resources/css/bootstrap/bootstrap.min.css" rel="stylesheet" />

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="resources/js/bootstrap/bootstrap.min.js"></script>
<link href="resources/css/cssapp.css" rel="stylesheet"/>
</head>
<body>
<div class = "container-fluid">
	<nav class="navbar navbar-expand-sm bg-light navbar-light">
	  <ul class="navbar-nav mr-auto">
	  <li class="nav-item ">
	      <a class="nav-link" href=" ./home">Home</a>
        </li>
	    <li class="nav-item ">
	      <a class="nav-link " href=" ./estudiante">Agregar estudiante</a>
        </li>
        <li class="nav-item ">
	      <a class="nav-link" href=" ./buscarEstudiante">Buscar estudiante</a>
        </li>
        <li class="nav-item active">
	      <a class="nav-link" href=" ./listarEstudiantes">Estudiantes ingresados</a>
        </li>
	  </ul>
	  <ul class ="navbar-nav">
        <li class="nav-item" style="text-align:right;" >
	      <a class="nav-link " href="./">Salir</a>
        </li>  
	  </ul>
	</nav>
</div>

<br>


<div class = "container" style="height:800px">

	<div class="form-row col-md-12">
             <h4 style="margin-left: -15px;">Estudiantes ingresados</h4>      
     </div>
     <br> 
 	<table class="table">
	  <thead>
	    <tr>
	      <th scope="col">ID</th>
	      <th scope="col">Estudiante</th>
	      <th scope="col">Email</th>
	      <th scope="col">Lenguajes</th>
	      <th scope="col">OS</th>
	      <th scope="col">Ciudad</th>
	      <th scope="col"><span style="font-size:25px;line-height:0px;margin-left:40px;text-align:center;">&#x2699;</span>
	      </th>
	    </tr>
	  </thead>
	  <tbody>
	  <c:forEach items="${estudiantes}" var="es">
	  	<tr>
	  	  <th scope="row">${es.id_estudiante }</th>
	      <td >${es.nombre} ${es.apellido }</td>
	      <td>${es.email}</td>
	      <td>
	      	<c:forEach var = "e" items = "${es.lenguajePro}">
                     <c:out value = "${e}"/> 
      		</c:forEach>
	      </td>
	      
	      <td>${es.sistemaOp }</td>
	      <td>${es.ciudad }</td>
	      <td>
	      <form:form action="modEstudiante" method='post' modelAttribute="estudiante">
	      	<form:hidden path = "id_estudiante" value = "${es.id_estudiante}" />
	      	<form:button class="btn btn-light col-md-12" style="font-size:12px;" ><b>Editar</b></form:button>
	      </form:form>
	      	
	      </td>
	    </tr>
      </c:forEach>
	  </tbody>
	</table>
 </div>

</body>
</html>