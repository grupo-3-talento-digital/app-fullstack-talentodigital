<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
	<title>Home</title>
	<link href="resources/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
<div class = "container-fluid">
	<nav class="navbar navbar-expand-sm bg-light navbar-light">
	  <ul class="navbar-nav mr-auto">
	    <li class="nav-item ">
	      <a class="nav-link active" href=" ./home">Home</a>
        </li>
         <li class="nav-item ">
	      <a class="nav-link " href="./estudiante">Agregar estudiante</a>
        </li>
        <li class="nav-item ">
	      <a class="nav-link" href=" ./buscarEstudiante">Buscar estudiante</a>
        </li>    
      	 <li class="nav-item ">
	      <a class="nav-link" href=" ./listarEstudiantes">Estudiantes ingresados</a>
        </li>  
      
        </ul>
     <ul class ="navbar-nav">
        <li class="nav-item" style="text-align:right;" >
	      <a class="nav-link " href="./">Salir</a>
        </li>  
	  </ul>
	</nav>
</div>
<br>
<h1 class="text-center">
	Bienvenido ${admin.usuario }
</h1>

<div class="container text-center">
	<p>Para agregar un nuevo estudiante haz click en el siguiente boton</p>
	<a href="./estudiante"><button type="submit" class="btn btn-light">Nuevo estudiante</button></a>
</div>




</body>
</html>
