package com.fullstack.appGrupo3.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.fullstack.appGrupo3.modelo.Estudiante;

public class EstudianteMapper implements RowMapper<Estudiante> {

	@Override
	public Estudiante mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Estudiante alumno = new Estudiante();
		alumno.setId_estudiante(rs.getInt("idEstudiante"));
		alumno.setNombre(rs.getString("nombre"));
		alumno.setApellido(rs.getString("apellido"));
		alumno.setCiudad(rs.getString("ciudad"));
		alumno.setEmail(rs.getString("email"));
		//REVISAR
		String lenguajes = rs.getString("lenguajesProg");
		
		String[] result = lenguajes.split(" ");

        List<String> listLeng = new ArrayList<String>(); 
		for(String r: result) {
		    if(r.equals(" ")){
		        System.out.println(r);
		    }else{
		        listLeng.add(r);
		    }
		}
		alumno.setLenguajePro(listLeng);
		alumno.setSistemaOp(rs.getString("sistemaOperativo"));
		return alumno;
		
	
		
	}
}
	
