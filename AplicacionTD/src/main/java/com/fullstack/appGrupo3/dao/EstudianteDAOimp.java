package com.fullstack.appGrupo3.dao;

import java.sql.Types;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;


import com.fullstack.appGrupo3.modelo.Estudiante;
import com.fullstack.appGrupo3.modelo.EstudianteDAO;
import com.fullstack.appGrupo3.config.DBconfig;
import com.fullstack.appGrupo3.mapper.EstudianteMapper;

public class EstudianteDAOimp implements EstudianteDAO {
	
	
	private JdbcTemplate jt;
	private NamedParameterJdbcTemplate npjt;
	
public EstudianteDAOimp() {
		
		DBconfig dataSource = new DBconfig();
		jt = new JdbcTemplate(dataSource.getDataSource());
		npjt = new NamedParameterJdbcTemplate(dataSource.getDataSource());
		
	}

	@Override
	public void create(Estudiante alumno) {
		String query = "insert into estudiante(nombre,apellido,email,ciudad,lenguajesProg,sistemaOperativo) values(:nombre,:apellido,:email,:ciudad,:lenguajesProg,:sistemaOperativo)";
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		
		params.addValue("nombre", alumno.getNombre(), Types.VARCHAR);
		params.addValue("apellido", alumno.getApellido(), Types.VARCHAR);
		params.addValue("email", alumno.getEmail(),Types.VARCHAR);
		params.addValue("ciudad", alumno.getCiudad(), Types.VARCHAR);
	

		//REVISAR TIPO
		String lenguajeSQL = "";
		List<String> lenguajes = alumno.getLenguajePro();
		for(String lenguaje: lenguajes) {
			lenguajeSQL += lenguaje+" ";
		}
	
		params.addValue("lenguajesProg",lenguajeSQL, Types.VARCHAR);
		params.addValue("sistemaOperativo", alumno.getSistemaOp(), Types.VARCHAR);
		npjt.update(query, params);
		
	}

	@Override
	public List<Estudiante> readAll() {
		String query = "select idEstudiante, nombre, apellido, email, ciudad, lenguajesProg, sistemaOperativo from estudiante";	
		return jt.query(query, new EstudianteMapper());
	
	}

	@Override
	public Estudiante read(int id) {
		String query = "select idEstudiante, nombre, apellido, email, ciudad, lenguajesProg, sistemaOperativo from estudiante where idEstudiante = ? ";
		return jt.queryForObject(query, new Object[] { id }, new EstudianteMapper());
				
				
			
	
	}

	@Override
	public void update(Estudiante alumno) {
		String query = "UPDATE estudiante SET nombre = ?,apellido=?,email=?,ciudad=?,lenguajesProg=?,sistemaOperativo=? where idEstudiante = ?";
		
		MapSqlParameterSource params = new MapSqlParameterSource();
		
		
		System.out.println(alumno.getId_estudiante());
		params.addValue("nombre", alumno.getNombre(), Types.VARCHAR);
		params.addValue("apellido", alumno.getApellido(), Types.VARCHAR);
		params.addValue("email", alumno.getEmail(),Types.VARCHAR);
		params.addValue("ciudad", alumno.getCiudad(), Types.VARCHAR);
	
		//REVISAR TIPO
		String lenguajeSQL = "";
		List<String> lenguajes = alumno.getLenguajePro();
		for(String lenguaje: lenguajes) {
			lenguajeSQL += lenguaje+" ";
		}
	
		params.addValue("lenguajesProg",lenguajeSQL, Types.VARCHAR);
		params.addValue("sistemaOperativo", alumno.getSistemaOp(), Types.VARCHAR);
		params.addValue("idEstudiante", alumno.getId_estudiante(),Types.INTEGER);
		jt.update(query, alumno.getNombre(), alumno.getApellido(),alumno.getEmail(), alumno.getCiudad(),lenguajeSQL, alumno.getSistemaOp(), alumno.getId_estudiante());
		
	}

	@Override
	public void delete(int id) {
		
		String query ="DELETE FROM estudiante WHERE idEstudiante=?";
		jt.update(query,id);

	}

}
