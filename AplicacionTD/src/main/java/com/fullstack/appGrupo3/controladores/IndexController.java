package com.fullstack.appGrupo3.controladores;

import java.util.ArrayList;
import java.util.List;


import javax.validation.Valid;

//import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.fullstack.appGrupo3.dao.EstudianteDAOimp;
import com.fullstack.appGrupo3.modelo.Admin;
import com.fullstack.appGrupo3.modelo.Ciudad;
import com.fullstack.appGrupo3.modelo.Estudiante;


@Controller
public class IndexController {
	
	
	@ModelAttribute("estudiante")
	public Estudiante setEstudiante() {
		
		return new Estudiante();
	}
	
	@ModelAttribute("admin")
	public Admin setAdmin() {
		
		return new Admin();
	}

	@ModelAttribute("lenguajes")
	public List<String> lenguajes(){
		List<String> leng = new ArrayList<String>();
		leng.add("Java");
		leng.add("Python");
		leng.add("C++");
		leng.add("Ruby");
		return leng;
	}
	
	@ModelAttribute("sistemas")
	public List<String> sistemas(){
		List<String> sis = new ArrayList<String>();
		sis.add("Windows 10");
		sis.add("Mac OS");
		sis.add("Linux");
		return sis;
		
	}
	
	@ModelAttribute("ciudades")
	public List<String> ciudades(){
		
		Ciudad c1 = new Ciudad(1,"Arica");
		Ciudad c2 = new Ciudad(2,"Iquique");
		Ciudad c3 = new Ciudad(3,"Calama");
		Ciudad c4 = new Ciudad(4,"Copiapo");
		Ciudad c5 = new Ciudad(5,"Coquimbo");
		Ciudad c6 = new Ciudad(6,"Valparaiso");
		Ciudad c7 = new Ciudad(7,"Santiago");
		Ciudad c8 = new Ciudad(8,"Talca");
		Ciudad c9 = new Ciudad(9,"Puerto Varas");
		Ciudad c10 = new Ciudad(10,"Punta Arenas");
		
		ArrayList<Ciudad> ciudades = new ArrayList<Ciudad>();
		

		ciudades.add(c1);
		ciudades.add(c2);
		ciudades.add(c3);
		ciudades.add(c4);
		ciudades.add(c5);
		ciudades.add(c6);
		ciudades.add(c7);
		ciudades.add(c8);
		ciudades.add(c9);
		ciudades.add(c10);
		
		List<String> cities = new ArrayList<String>();
		
		for(Ciudad c: ciudades) {
			cities.add(c.getNombre());
		}
		
		return cities;
	}
	
	@RequestMapping("/buscarEstudiante")
	public String getBuscarEstudiante() {
		return "buscarEstudiante";
	}
	
	@RequestMapping("/home")
	public String getHome() {	
		return "home";
	}
	
	@RequestMapping("/")
	public String getLogin() {
		return "login";
	}
	
	@RequestMapping(value="loginAdmin",method=RequestMethod.POST)
	public String ingresarAdmin(@Valid @ModelAttribute("admin") Admin adm, BindingResult br, Model model) {
		if(br.hasErrors()) {
			return "/login";
			
		}else {
			if(adm.getUsuario().equals("admin") && adm.getPassword().equals("12345")) {
				
				return "home";
			}
			else {
				return "error";
			}
			
		}
	}
		
	
	@RequestMapping("/estudiante")
	public ModelAndView getResumen() {
		
		ModelAndView m = new ModelAndView();
		
		m.setViewName("estudiante");
		
		return m;
	}
	
	
	@RequestMapping(value="regEstudiante",method=RequestMethod.POST)
	public String agregarEstudiante(@Valid @ModelAttribute("estudiante") Estudiante alumno, BindingResult br, Model model) {
		
		if(br.hasErrors()) {
			return "estudiante";
			
		}else {

			EstudianteDAOimp edao = new EstudianteDAOimp();
			edao.create(alumno);
			model.addAttribute("estudiante",alumno);
			List<Estudiante> estudiantes = edao.readAll();
			model.addAttribute("estudiantes", estudiantes);
			return "correcto";
		}
		
	}
	
	@RequestMapping(value="searchStudent", method=RequestMethod.POST)
	public  String buscarEstudiante(@ModelAttribute("estudiante") Estudiante alumno, Model model) {
		EstudianteDAOimp edao = new EstudianteDAOimp();
		
		try {
			Estudiante alumnoBuscado = edao.read(alumno.getId_estudiante());
			model.addAttribute("estudianteBuscado",alumnoBuscado);
			return "buscarEstudiante";
			
		}
		catch(Exception e) {
			e.getMessage();
			String mensaje = "El alumno buscado no existe.";
			model.addAttribute("mensajeError", mensaje);
			return "buscarEstudiante";
		}
		
		
		
	}
	
	@RequestMapping(value="modEstudiante",method=RequestMethod.POST)
	public ModelAndView modificarEstudiante(@ModelAttribute("estudiante") Estudiante alumno) {
		EstudianteDAOimp edao = new EstudianteDAOimp();
		Estudiante alumnoBuscado = edao.read(alumno.getId_estudiante());
		ModelAndView m = new ModelAndView();
		List<String> noChecked = new ArrayList<String>();
		String buttonSelected = alumnoBuscado.getSistemaOp();
		List<String> noButtons = new ArrayList<String>();
		
		for(String leng: lenguajes()) {
			if(!alumnoBuscado.getLenguajePro().contains(leng)) {
				noChecked.add(leng);		
			}
		}
		
		for(String sist: sistemas()) {
			if(!buttonSelected.equals(sist)) {
				noButtons.add(sist);

			}
		}

		m.addObject("nocheckeadosbutton", noButtons);
		m.addObject("nocheckeados", noChecked);
		m.addObject("editEstudiante",alumnoBuscado);
		m.setViewName("modificarEstudiante");
		return m;
	}
	
	
	
	
	@RequestMapping(value="updateEstudiante",method=RequestMethod.POST)
	public String updateEstudiante(@Valid @ModelAttribute("estudiante") Estudiante alumno, BindingResult br, Model model) {
		if(br.hasErrors()) {
			EstudianteDAOimp edao = new EstudianteDAOimp();
			Estudiante alumnoBuscado = edao.read(alumno.getId_estudiante());
			String buttonSelected = alumnoBuscado.getSistemaOp();

			List<String> noChecked = new ArrayList<String>();
			List<String> noButtons = new ArrayList<String>();

			
			for(String leng: lenguajes()) {
				if(!alumnoBuscado.getLenguajePro().contains(leng)) {
					noChecked.add(leng);		
				}
			}
			
			for(String sist: sistemas()) {
				if(!buttonSelected.equals(sist)) {
					noButtons.add(sist);

				}
			}
			model.addAttribute("editEstudiante", alumnoBuscado);
			model.addAttribute("nocheckeados", noChecked);
			model.addAttribute("nocheckeadosbutton", noButtons);
			return "modificarEstudiante";
			
		}else {

			EstudianteDAOimp edao = new EstudianteDAOimp();
			edao.update(alumno);
			model.addAttribute("estudiante",alumno);
			
			return "editCorrecto";
		}
	}
	
	@RequestMapping(value="deleteEstudiante",method=RequestMethod.POST)
	public String deleteEstudiante(@ModelAttribute("estudiante") Estudiante alumno, BindingResult br, Model model) {
		
		EstudianteDAOimp edao = new EstudianteDAOimp();
		int id = alumno.getId_estudiante();
		edao.delete(id);
		
		List<Estudiante> estudiantes = edao.readAll();
		model.addAttribute("estudiantes", estudiantes);

		return "/listarEstudiantes";
	}
	
	@RequestMapping("/listarEstudiantes")
	public ModelAndView getEstudiantes() {
		EstudianteDAOimp edao = new EstudianteDAOimp();
		ModelAndView m = new ModelAndView();
		List<Estudiante> estudiantes = edao.readAll();
		m.addObject("estudiantes",estudiantes);
		m.setViewName("listarEstudiantes");
		return m;
	}
	
}
