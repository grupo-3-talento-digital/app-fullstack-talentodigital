package com.fullstack.appGrupo3.modelo;

import java.util.List;

public interface EstudianteDAO {
	
	void create(Estudiante alumno);
	
	Estudiante read(int id);
	
	List<Estudiante> readAll();

	void update(Estudiante alumno);
	
	void delete(int id);

}
