<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Inicie sesion</title>
<link href="resources/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="resources/js/bootstrap/bootstrap.min.js"></script>
<link href="resources/css/cssapp.css" rel="stylesheet"/>
</head>
<body>
<div class = "container-fluid">
	<nav class="navbar navbar-expand-sm bg-light navbar-light">
	  <ul class="navbar-nav">
	  <li class="nav-item ">
	      <a class="nav-link" href=" ./">Home</a>
        </li>
	  </ul>
	</nav>
</div>


<div class ="container ">
        <form:form action="loginAdmin" method='post' modelAttribute="admin" class = "formLogin" >
        
            
            <br>
	
                
        <div class="form-row col-md-12 text-center">
                    <h4 >Login</h4>
                  
        </div> 
                
		<div class="form-row col-md-12">
			
			<div class="form-group col-md-12 col-12" >
				<label class="col-form-label">Usuario: </label><form:input path="usuario"  class="form-control col-md-12" />
				<form:errors path="usuario" cssClass="error"></form:errors>
				
				
			</div>

			<div class="form-group col-md-12 col-12">
				<label class="col-form-label">Password: </label><form:password path="password" class="form-control col-md-12"  />
				<form:errors path="password" cssClass="error"></form:errors>
			</div>
			

			<div class="form-group col-md-12 col-12">
				 <form:button class="btn btn-light col-12 col-md-12 " style="margin-top:38px;">Ingresar</form:button>
			</div>	
			
		</div>
			
		</form:form>
</div>

</body>
</html>