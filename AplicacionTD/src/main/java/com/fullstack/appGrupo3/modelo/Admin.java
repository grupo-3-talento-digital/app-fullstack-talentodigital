package com.fullstack.appGrupo3.modelo;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class Admin {

	
	private int id;
	@NotEmpty(message = "Debe rellenar el campo")
	@Size( max = 30, message ="El campo debe tener entre 1 y 30 caracteres" )
	private String usuario;
	@Size( max = 8, message ="El campo debe tener entre 1 y 8 caracteres" )
	@NotEmpty(message = "Debe rellenar el campo")
	private String password;
	
	public Admin() {}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getUsuario() {
		return usuario;
	}
	
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password=password;
	}
	
	
	
	

}
