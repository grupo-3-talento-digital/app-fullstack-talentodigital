<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Buscar estudiante</title>
<link href="resources/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="resources/js/bootstrap/bootstrap.min.js"></script>
<link href="resources/css/cssapp.css" rel="stylesheet"/>
</head>
<body>
<div class = "container-fluid">
	<nav class="navbar navbar-expand-sm bg-light navbar-light">
	  <ul class="navbar-nav mr-auto">
	  <li class="nav-item ">
	      <a class="nav-link" href=" ./home">Home</a>
        </li>
	    <li class="nav-item ">
	      <a class="nav-link " href=" ./estudiante">Agregar estudiante</a>
        </li>
        <li class="nav-item ">
	      <a class="nav-link active" href=" ./buscarEstudiante">Buscar estudiante</a>
        </li>
        <li class="nav-item ">
	      <a class="nav-link" href=" ./listarEstudiantes">Estudiantes ingresados</a>
        </li>
	  </ul>
	  <ul class ="navbar-nav">
        <li class="nav-item" style="text-align:right;" >
	      <a class="nav-link " href="./">Salir</a>
        </li>  
	  </ul>
	</nav>
</div>

<div class="container-fluid" style="width:400px; height:400px; margin:0 auto;">

<br>
<br>

 	<form:form action="searchStudent"  method='post' modelAttribute="estudiante" id = "formSearchStudent">
 
 	<div class="form-row">
 
 		<div class="form-group col-md-6">
				<label class="col-form-label">Ingrese id estudiante: </label><form:input path="id_estudiante" id = "idBuscado" class="form-control col-md-12" required="required"/>
				<form:errors path="id_estudiante" cssClass="error"></form:errors>
				
		</div>
		
		<div class="form-group col-md-6">
			<form:button class="btn btn-dark col-md-12" id="buscarAlumno " style="margin-top:38px;">Buscar estudiante</form:button>
		</div>
		
		
		
	</div>
 	
	 </form:form>
 
 
 
	<br>    
     <div id = "infoEstudiante" >
        
        <p>
        
        
        
        <c:choose  >
        	<c:when test="${estudianteBuscado != null}">
        	
        		
        		<div class="form-row col-md-12">
             		<h4 style="margin-left: -15px;">Datos Estudiante</h4>      
       		 	</div> 
        			Nombre:  ${estudianteBuscado.nombre} <br>

		            Apellido: ${estudianteBuscado.apellido} <br>
		            
		            Email: ${estudianteBuscado.email}<br>
		
		            Ciudad: ${estudianteBuscado.ciudad}<br>
		
		            Lenguajes de programación predilectos: <br>
		            
		            <c:forEach var = "i" items = "${estudianteBuscado.lenguajePro}">
		                     ${i}
		      		</c:forEach>
		      		
		      		 <br>
		
		            Sistema operativo predilecto: ${estudianteBuscado.sistemaOp}<br>
		            
		            
		            <br>
		            
		            <form:form action="modEstudiante" method='post' modelAttribute="estudiante">
	      				<form:hidden path = "id_estudiante" value = "${es.id_estudiante}" />
	      				<form:button class="btn btn-dark col-md-12" style="font-size:15;" >&#x2699; Editar</form:button>
	    			</form:form>
		            
        	</c:when>
        	
        	<c:otherwise>
        		${mensajeError}
        	</c:otherwise>
        </c:choose>
        
        	
        	
           
            
        </p>
    </div>


</div>




</body>
<script src="resources/js/bootstrap/buscarestudiante.js"></script>
</html>