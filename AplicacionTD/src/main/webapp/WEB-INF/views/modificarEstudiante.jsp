<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>modificar estudiante</title>
<link href="resources/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="resources/js/bootstrap/bootstrap.min.js"></script>
<link href="resources/css/cssapp.css" rel="stylesheet"/>
</head>
<body>
<div class = "container-fluid">
	<nav class="navbar navbar-expand-sm bg-light navbar-light">
	  <ul class="navbar-nav mr-auto">
	  <li class="nav-item ">
	      <a class="nav-link" href=" ./home">Home</a>
        </li>
	    <li class="nav-item ">
	      <a class="nav-link " href=" ./estudiante">Agregar estudiante</a>
        </li>
        <li class="nav-item ">
	      <a class="nav-link " href=" ./buscarEstudiante">Buscar estudiante</a>
        </li>
        <li class="nav-item active">
	      <a class="nav-link" href=" ./listarEstudiantes">Estudiantes ingresados</a>
        </li>
	  </ul>
	  <ul class ="navbar-nav">
        <li class="nav-item" style="text-align:right;" >
	      <a class="nav-link " href="./">Salir</a>
        </li>  
	  </ul>
	</nav>
</div>

<div class ="container">

		


        <form:form action="updateEstudiante" method='post' modelAttribute="estudiante" id = "formUpdateEstudiante">
        
        <br>
	
                
	        <div class="form-row col-md-12">
	                    <h4 style="margin-left: -15px;">Datos Estudiante</h4>
	                  
	        </div> 
	                
			<div class="form-row">
				
				<form:hidden path = "id_estudiante" value = "${editEstudiante.id_estudiante}" />
	
				<div class="form-group col-md-6 ">
					<label class="col-form-label">Nombre: </label><form:input path="nombre"  class="form-control col-md-11" value="${editEstudiante.nombre}"/>
					<form:errors path="nombre" cssClass="error"></form:errors>
					
					
				</div>
	
				<div class="form-group col-md-6 ">
					<label class="col-form-label">Apellido: </label><form:input path="apellido" class="form-control col-md-11" value="${editEstudiante.apellido}"  />
					<form:errors path="apellido" cssClass="error"></form:errors>
				</div>
				
				
				<div class="form-group col-md-6 ">
					<label class="col-form-label">Email: </label><form:input  path="email" class="form-control col-md-11"  value="${editEstudiante.email}"/>
					<form:errors path="email" cssClass="error"></form:errors>
				</div>
				
				
				
	                    
	            <div class="form-group col-md-6 ">
					<label class="col-form-label">Ciudad de residencia</label><form:select path="ciudad" class="form-control col-md-11"  >
						<form:option selected="false" value="" label="Elija ciudad..."/>
						<form:option selected="true" value="${editEstudiante.ciudad}" label="${editEstudiante.ciudad}"/>
						<form:options items="${ciudades}" />
					</form:select>
					<form:errors path="ciudad" cssClass="error"></form:errors>
				</div>
				
				<div class ="form-group col-md-4 ">
					<label class="col-form-label">Lenguajes de programación favoritos</label>
					<div class="form-control col-md-7 col-12" style="height:110px;">
					
						<c:forEach var = "i" items = "${editEstudiante.lenguajePro}">
						
	                     	<form:checkbox path ="lenguajePro" value="${i}" checked="checked"/> ${i}<br>
	      				</c:forEach>
						
						<c:forEach var = "s" items = "${nocheckeados}">
						
	                     	<form:checkbox path ="lenguajePro" value="${s}"/> ${s}<br>
	                    </c:forEach> 	
						
					</div>
					<form:errors path="lenguajePro" cssClass="error"></form:errors>
	
	
	
				</div>
	
	            
	            <div class="form-group col-md-4">
	                
	            	<label class="col-form-label">Sistema operativo</label>
	            	<div class="form-control col-md-7 col-12" style="height: 90px;">
	            	
	            		
	            		
	            		<form:radiobutton path="sistemaOp" value="${editEstudiante.sistemaOp}" checked="checked"/> ${editEstudiante.sistemaOp}<br>
	            		<c:forEach var = "e" items = "${nocheckeadosbutton}">
	                     	<form:radiobutton path="sistemaOp" value="${e}"/> ${e}<br>  
	      				</c:forEach>
	            		
	            	</div>
	            	<form:errors path="sistemaOp" cssClass="error"></form:errors>
	            		 
	            </div>
	
				
				  
	
	
				<div class="form-group col-md-12 col-12">
					<form:button class="btn btn-dark mb-4 col-md-2" id="updateButton">Actualizar datos</form:button> 
				</div>	
				
			</div>
			
		</form:form>
		
		<form:form action="deleteEstudiante" method='post' modelAttribute="estudiante" id = "formDeleteEstudiante">
					<form:hidden path = "id_estudiante" value = "${editEstudiante.id_estudiante}" />
					<form:button class="btn btn-danger mb-4" id="deleteButton" style="margin-right:40px; margi-bottom:60px;">&#x233d; Eliminar estudiante</form:button>
				</form:form>	 
</div>

	





</body>
<script src="resources/js/bootstrap/delete.js"></script>

</html>